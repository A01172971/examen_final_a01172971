defmodule TaxiBeWeb.TaxiAllocationJob do
  use GenServer

  def start_link(request, name) do
    GenServer.start_link(__MODULE__, request, name: name)
  end

  def init(request) do
    # Process.send(self(), :step1, [:nosuspend])
    {:ok, %{request: request}}
  end

  def handle_info(:step1, %{request: _request} = state) do
    IO.puts("Hello from step1")
    {:noreply, state}
  end

  def handle_info(:case0, %{candidates: taxis, request: request} = state) do
    IO.inspect(state)
    taxi1 = hd(taxis)
    taxi2 = hd(tl(taxis))

    %{
      "pickup_address" => pickup_address,
      "dropoff_address" => dropoff_addreess,
      "booking_id" => booking_id
    } = request
    TaxiBeWeb.Endpoint.broadcast(
      "driver:" <> taxi1.nickname,
      "booking_request",
      %{
        msg: "viaje de '#{pickup_address} a '#{dropoff_address}'",
        bookingId: booking_id
      })
    TaxiBeWeb.Endpoint.broadcast(
      "driver:" <> taxi2.nickname,
      "booking_request",
      %{
        msg: "viaje de '#{pickup_address} a '#{dropoff_address}'",
        bookingId: booking_id
      })
      Process.send_after(self(), :case1, 2_000)
      {:noreply, state}
  end

  def handle_info(:case1, %{candidates: taxis, countReject: count, request: request} = state) do
    IO.inspect(state)
    taxi1 = hd(taxis)
    taxi2 = hd(tl(taxis))
    if count < 2 do
      Process.send(self(), :case0, [:nosuspend])
    else
      TaxiBeWeb.Endpoint.broadcast(
      "customer:" <> customer,
      "booking_request",
      %{
        msg: "Cannot find a taxi for you"
      })
    end
    {:noreply, state}
  end

  def handle_info(:case2, %{candidates: taxis, countaccept: count, request: request} = state) do
    IO.inspect(state)
    taxi1 = hd(taxis)
    taxi2 = hd(tl(taxis))
    if count > 0 do
      Process.send(self(), :case0, [:nosuspend])
    else
      TaxiBeWeb.Endpoint.broadcast(
      "customer:" <> customer,
      "booking_request",
      %{
        msg: "Taxi is on its way"
      })
    end
    {:noreply, state}
  end

  def handle_info(:case3, %{candidates: taxis, timer: timer, countNore: count} = state) do
    IO.inspect(state)
    taxi1 = hd(taxis)
    taxi2 = hd(tl(taxis))
    if count < 0 do
      TaxiBeWeb.Endpoint.broadcast(
      "customer:" <> customer,
      "booking_request",
      %{
        msg: "Cannot find a taxi for you"
      })
      Process.send_after(self(), :case3, 120_000)
      {:noreply, state |> Map.put(count + 1)}
    else
      Process.send(self(), :case0, [:nosuspend])
    end
  end

  def handle_cast({:driver_rejected, username}, state) do
    {:noreply, state}
  end

  def handle_cast({:driver_accepted, username}, state) do
    {:noreply, state}
  end

  def select_candidate_taxis() do
    [
      %{nickname: "frodo", latitude: 19.0319783, longitude: -98.2349368}, # Angelopolis
      %{nickname: "pipin", latitude: 19.0061167, longitude: -98.2697737}, # Arcangeles
      %{nickname: "merry", latitude: 19.0092933, longitude: -98.2473716} # Paseo Destino
    ]
  end
end
